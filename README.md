# Große Übersicht
- [x] [How To Plenum](markdown/Plenum.md)
    - [x] markdown
    - [x] svg
    - [x] pdf

# Methoden
- [ ] Fishbowl
    - [ ] markdown
    - [ ] svg
    - [ ] pdf
- [ ] Murmelrunden
    - [ ] markdown
    - [ ] svg
    - [ ] pdf
- [ ] Schriftsammlung
    - [ ] markdown
    - [ ] svg
    - [ ] pdf
- [ ] Stimmungsbild
    - [ ] markdown
    - [ ] svg
    - [ ] pdf
- [ ] Emo Runde
    - [ ] markdown
    - [ ] svg
    - [ ] pdf

# Erläuterungen
- [ ] Konsens (mehrstufig)

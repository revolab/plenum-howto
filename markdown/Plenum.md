# Das Plenum

*ist die Vollversammlung möglichst aller Mitglieder einer Gruppierung.*

## Inhalt

Im Plenum werden alle möglichen Sachverhalte besprochen welche die Gruppe betreffen oder von ihr getragen werden müssen. Während der Vorbereitung wird eine TO (Tagesordnung) erstellt. Gerne kannst du bei dieser Vorbereitung mithelfen und das Plenum mitgestalten. Die Reihenfolge und eventuelle Ergänzungen werden am Anfang des Plenums mit allen abgeklärt.

## Moderation

Zur Koordination des Plenums gibt es eine oder mehrere Modertor*innen. Diese haben die Aufgabe das Gespräch Zielführend zu gestalten, indem sie verschiedene Methoden zur Entscheidungsfindung einsetzen und Redezeiten begrenzen können. Dem Plenum ist es aber jederzeit möglich zu intervenieren, wenn die Moderation in irgendeiner Form, bewusst oder unbewusst, inakzeptabel agiert. Probiere am besten selbst einmal aus zu moderieren. Melde dich dafür einfach am Ende des Plenums.

## Redeliste

Wir verwenden eine doppelt quotierte Redeliste um die Redeanteile möglichst fair zu verteilen. Das bedeutet, dass nicht Cis-männliche Personen und Erstredner*innen in der Reihenfolge bevorzugt werden. Bitte rede nur, wenn du von der Moderation aufgerufen wirst.

## Beiträge

Versuche deine Beiträge so kurz wie möglich zu halten. Höre bei den anderen genau hin, vielleicht hat jemensch schon das gesagt was du einbringen wolltest. In diesem Fall kannst du deine Meldung auch wieder zurückziehen. (Eine Wiederholung bringt der Diskussion nichts und kostet nur Zeit.) Beachte die TO und bringe deine Beiträge nur dann ein, wenn sie inhaltlich passen. (Tipp: Am Ende der TO gibt es in der Regel einen „Sonstiges“ Punkt, bei dem du deinen Beitrag unterbringen kannst.) 

## Entscheidungen

Entscheidungen werden bei uns im Konsensprinzip getroffen. Das heißt, ein Vorschlag wird nur dann angenommen, wenn es kein Veto dagegen gibt. Bei komplexeren Fragen wird ein mehrstufiges Verfahren eingesetzt.

## Handzeichen

| <img src="/img/single.png" width="100" > | <img src="/img/double.png" width="100"> | <img src="/img/P.png" width="100">         | <img src="/img/T.png" width="100">            |<img src="/img/shake-up.png" width="100"> | <img src="/img/shake-down.png" width="100"> | <img src="/img/X.png" width="100"> | <img src="/img/circle.png" width="100"> |
| ---------------------------------------- | --------------------------------------- | ------------------------------------------ | --------------------------------------------- | ---------------------------------------- | ------------------------------------------- |----------------------------------- | --------------------------------------- |
| Einfache Meldung für einen neuen Beitrag | Meldung direkt zum vorherigen Beitrag   | Prozess Vorschlag ("Stimmungs-bild bitte") | technischer Punkt ("Fenster schließen bitte") | Zustimmung (oben wedeln)                 | Ablehnung (unten wedeln)                    | Veto (überkreuzte Arme)            | Wiederholung ("Komm mal auf den Punkt") |

## Protokoll

Es gibt immer eine Mitschrift, in der die wichtigsten Punkte des Plenums festgehalten werden. Diese Aufgabe wird am Anfang vergeben und kann auch gerne von dir übernommen werden. 

___

"How To Plenum" cc-by-sa-nc Alius  | Online: gitlab.com/revolab/plenum-howto

